import java.util.*;
public class Task07_PhoneNumbers {

    public static void main(String[] args) {
        new Task07_PhoneNumbers();
    }
    Task07_PhoneNumbers() {
        Scanner sc = new Scanner(System.in);
        int numOfCountries = sc.nextInt();
        int phoneInfo[][] = new int[numOfCountries][101];
        for (int i = 0; i < numOfCountries; i++) {
            phoneInfo[i][0] = sc.nextInt();
            int numOfRegions = sc.nextInt();
            for (int j = 1; j <= numOfRegions; j++) phoneInfo[i][j] = sc.nextInt();
        }
        int numOfPhoneNumbers = sc.nextInt();
        String phoneNumbers[] = new String[numOfPhoneNumbers];
        String phoneBook[] = new String[numOfPhoneNumbers];
        for (int i = 0; i < numOfPhoneNumbers; i++) {
            String phoneNumber = sc.next();
            String phoneNumberR = "";
            for (int j = 0; j < numOfCountries; j++) {
                String cCode = phoneInfo[j][0];
                if (phoneNumber.startsWith(cCode)) {
                    int cCodeLength = cCode.length();
                    for (int k = 0; k < 101 & phoneInfo[j][k] != 0; k++) {
                        String rCode = phoneInfo[j][k];
                        if (phoneNumber.substring(cCodeLength, 11).startsWith(rCode)) {
                            phoneNumberR += "+" + cCode + "(" + rCode + ")";
                            int rCodeLength = rCode.length();
                            String other = phoneNumber.substring(cCodeLength + rCodeLength, 11);
                            if (other.startsWith("0")) phoneNumberR = "";
                            else {
                                int otherLength = other.length();
                                switch (otherLength) {
                                    case 3:
                                        phoneNumberR += other;
                                        break;
                                    case 4:
                                        phoneNumberR += other.substring(0, 2) + "-" + other.substring(2, 4);
                                        break;
                                    case 5:
                                        phoneNumberR += other.substring(0, 3) + "-" + other.substring(3, 5);
                                        break;
                                    case 6:
                                        phoneNumberR += other.substring(0, 2) + "-" + other.substring(2, 4) + other.substring(4, 6);
                                        break;
                                    case 7:
                                        phoneNumberR += other.substring(0, 3) + "-" + other.substring(3, 5) + other.substring(5, 7);
                                        break;
                                }
                            }
                        }
                    }
                }
            }
             if (phoneNumberR == "") phoneNumberR = "Incorrect";
             System.out.println(phoneNumberR);
        }
        sc.close();
    }

}